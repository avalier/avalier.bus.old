﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Bus : Avalier.Bus.Base.Bus
    {
        private MessagingFactory _messagingFactory = null;

        public Bus()
            : base()
        {
        }

        internal MessagingFactory MessagingFactory
        {
            get { return _messagingFactory; }
        }

        public override IConfiguration CreateConfiguration(IBus bus)
        {
            return new Configuration(bus);
        }

        protected override void InitialiseBus()
        {
            _messagingFactory = MessagingFactory.CreateFromConnectionString(this.Configuration.ConnectionString);
        }
    }
}
