﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Publication : Avalier.Bus.Base.Publication
    {
        private TopicClient _topicClient = null;

        public Publication(IConfiguration configuration, string topicName)
            : base(configuration, topicName)
        {
        }

        public override void Initialise()
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;

            var namespaceManager = NamespaceManager.CreateFromConnectionString(configuration.ConnectionString);

            //namespaceManager.DeleteTopic(this.TopicName);
            if (!namespaceManager.TopicExists(this.TopicName))
            {
                namespaceManager.CreateTopic(new TopicDescription(this.TopicName)
                {
                    //RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Publication({0}): Initialisation: Created topic: {0}", this.TopicName));
            }
            _topicClient = bus.MessagingFactory.CreateTopicClient(this.TopicName);

            Console.WriteLine(string.Format("[ Bus ] Publication({0}): Initialised", this.TopicName)); ;
        }

        public override void Publish<TNotification>(TNotification notification)
        {
            var message = new BrokeredMessage(notification.ToXml())
            {
                CorrelationId = typeof(TNotification).AssemblyQualifiedName
            };
            _topicClient.Send(message);
            Console.WriteLine(string.Format("[ Bus ] {2}({1}) Publication({0}): Notification Published", this.TopicName, typeof(TNotification).Name, message.MessageId));
        }
    }
}
