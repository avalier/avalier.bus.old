﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Requestor : Avalier.Bus.Base.Requestor
    {
        private QueueClient _requestQueueClient = null;
        private QueueClient _responseQueueClient = null;

        public Requestor(IConfiguration configuration, string requestQueueName, string responseQueueName)
            : base(configuration, requestQueueName, responseQueueName)
        {
        }

        public override void Initialise()
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;

            var namespaceManager = NamespaceManager.CreateFromConnectionString(this.Configuration.ConnectionString);

            /*/
            namespaceManager.DeleteQueue(this.RequestQueueName);
            namespaceManager.DeleteQueue(this.ResponseQueueName);
            //*/

            if (!namespaceManager.QueueExists(this.RequestQueueName))
            {
                namespaceManager.CreateQueue(new QueueDescription(this.RequestQueueName)
                {
                    //RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Requester({0}, {1}): Initialisation: Created request queue: {0}", this.RequestQueueName, this.ResponseQueueName));
            }

            _requestQueueClient = bus.MessagingFactory.CreateQueueClient(this.RequestQueueName);
            if (!namespaceManager.QueueExists(this.ResponseQueueName))
            {
                namespaceManager.CreateQueue(new QueueDescription(this.ResponseQueueName)
                {
                    RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Requester({0}, {1}): Initialisation: Created response queue: {1}", this.RequestQueueName, this.ResponseQueueName));
            }
            _responseQueueClient = bus.MessagingFactory.CreateQueueClient(this.ResponseQueueName);

            Console.WriteLine(string.Format("[ Bus ] Requester({0}, {1}): Initialised", this.RequestQueueName, this.ResponseQueueName));
        }

        public override async Task<TResponse> Request<TRequest, TResponse>(TRequest request)
        {
            string body = "";
            var sessionId = Guid.NewGuid().ToString();

            // Request //
            var requestMessage = new BrokeredMessage(request.ToXml())
            {
                ReplyToSessionId = sessionId,
                CorrelationId = typeof(TRequest).AssemblyQualifiedName + ";" + typeof(TResponse).AssemblyQualifiedName
            };
            Console.WriteLine(string.Format("[ Bus ] {4}({2}:{3}) Requestor({0}, {1}): Requesting",
                this.RequestQueueName,
                this.ResponseQueueName,
                typeof(TRequest).Name,
                typeof(TResponse).Name,
                requestMessage.MessageId
            ));
            _requestQueueClient.Send(requestMessage);

            // Response //
            body = await _responseQueueClient.ReceiveSession(sessionId, this.Timeout);
            Console.WriteLine(string.Format("[ Bus ] {4}({2}:{3}) Requestor({0}, {1}): Responded",
                this.RequestQueueName,
                this.ResponseQueueName,
                typeof(TRequest).Name,
                typeof(TResponse).Name,
                requestMessage.MessageId
            ));

            return body.FromXml<TResponse>();
        }
    }
}
