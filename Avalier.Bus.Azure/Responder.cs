﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Responder : Avalier.Bus.Base.Responder
    {
        private QueueClient _requestQueueClient = null;
        private QueueClient _responseQueueClient = null;

        public Responder(IConfiguration configuration, string requestQueueName, string responseQueueName)
            : base(configuration, requestQueueName, responseQueueName)
        {
        }

        public override void Initialise()
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;

            var namespaceManager = NamespaceManager.CreateFromConnectionString(configuration.ConnectionString);
            if (!namespaceManager.QueueExists(this.RequestQueueName))
            {
                namespaceManager.CreateQueue(new QueueDescription(this.RequestQueueName)
                {
                    //RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Responder({0}, {1}): Initialisation: Created request queue: {0}", this.RequestQueueName, this.ResponseQueueName));

            }
            _requestQueueClient = bus.MessagingFactory.CreateQueueClient(this.RequestQueueName);
            if (!namespaceManager.QueueExists(this.ResponseQueueName))
            {
                namespaceManager.CreateQueue(new QueueDescription(this.ResponseQueueName)
                {
                    RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Responder({0}, {1}): Initialisation: Created response queue: {1}", this.RequestQueueName, this.ResponseQueueName));

            }
            _responseQueueClient = bus.MessagingFactory.CreateQueueClient(this.ResponseQueueName);

            _requestQueueClient.OnMessage(OnMessageArrived, new OnMessageOptions()
            {
                AutoComplete = true
            });

            Console.WriteLine(string.Format("[ Bus ] Responder({0}, {1}): Initialised", this.RequestQueueName, this.ResponseQueueName));
        }

        protected void OnMessageArrived(BrokeredMessage requestMessage)
        {
            var types = requestMessage.CorrelationId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            var requestType = Type.GetType(types[0]);
            var responseType = Type.GetType(types[1]);

            Console.WriteLine(string.Format("[ Bus ] {4}({2}:{3}) Responder({0}, {1}): Request Arrived",
                this.RequestQueueName,
                this.ResponseQueueName,
                requestType.Name,
                responseType.Name,
                requestMessage.MessageId
            ));

            var request = requestMessage.GetBody<string>().FromXml(requestType);
            var response = ((Base.Responder)this).Execute(requestType, responseType, requestMessage.MessageId, request);
            _responseQueueClient.Send(new BrokeredMessage(response.ToXml(responseType))
            {
                SessionId = requestMessage.ReplyToSessionId,
                MessageId = requestMessage.MessageId
            });

            Console.WriteLine(string.Format("[ Bus ] {4}({2}:{3}) Responder({0}, {1}): Request Completed",
                this.RequestQueueName,
                this.ResponseQueueName,
                requestType.Name,
                responseType.Name,
                requestMessage.MessageId
            ));
        }
    }
}
