﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Subscription : Avalier.Bus.Base.Subscription
    {
        private SubscriptionClient _subscriptionClient = null;

        public Subscription(IConfiguration configuration, string topicName, string subscriptionName)
            : base(configuration, topicName, subscriptionName)
        {
        }

        public override void Initialise()
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;

            var namespaceManager = NamespaceManager.CreateFromConnectionString(configuration.ConnectionString);

            namespaceManager.DeleteSubscription(this.TopicName, this.SubscriptionName);
            if (!namespaceManager.SubscriptionExists(this.TopicName, this.SubscriptionName))
            {
                namespaceManager.CreateSubscription(new SubscriptionDescription(this.TopicName, this.SubscriptionName)
                {
                    //RequiresSession = true
                });
                Console.WriteLine(string.Format("[ Bus ] Subscription({0}): Initialisation: Created subscription: {1} to topic: {0}", this.TopicName, this.SubscriptionName));
            }
            _subscriptionClient = bus.MessagingFactory.CreateSubscriptionClient(this.TopicName, this.SubscriptionName);
            _subscriptionClient.OnMessage(OnMessageArrived, new OnMessageOptions()
            {
                AutoComplete = true
            });
        }

        protected void OnMessageArrived(BrokeredMessage message)
        {
            var notificationType = Type.GetType(message.CorrelationId);

            Console.WriteLine(string.Format("[ Bus ] {3}({2}) Subscription({0}): Notification Arrived",
                this.TopicName,
                this.SubscriptionName,
                notificationType.Name,
                message.MessageId
            ));

            var request = message.GetBody<string>().FromXml(notificationType);
            this.Execute(notificationType, message.MessageId, request);

            Console.WriteLine(string.Format("[ Bus ] {3}({2}) Subscription({0}): Notification Completed",
                this.TopicName,
                this.SubscriptionName,
                notificationType.Name,
                message.MessageId
            ));
        }
    }
}
