﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    internal static class Extensions
    {
        public static void CreateQueueScope(this MessagingFactory messagingFactory, string queueName, Action<QueueClient> scope)
        {
            QueueClient q = null;
            try
            {
                q = messagingFactory.CreateQueueClient(queueName);
                scope(q);
            }
            finally
            {
                q.Close();
            }
        }

        public static bool Receive(this QueueClient q, Action<BrokeredMessage> scope)
        {
            using (var response = q.Receive(TimeSpan.FromMilliseconds(1000)))
            {
                if (null != response)
                {
                    scope(response);
                    response.Complete();
                    return true;
                }
            };
            return false;
        }

        public static async Task<string> ReceiveSession(this QueueClient q, string sessionId, int timeout)
        {

            MessageSession session = null;
            BrokeredMessage message = null;
            try
            {
                session = q.AcceptMessageSession(sessionId);
                message = await session.ReceiveAsync(TimeSpan.FromMilliseconds(timeout));
                if (null != message)
                {
                    var body = message.GetBody<string>();
                    message.Complete();
                    return body;
                }
                throw new System.TimeoutException("The response failed to return within " + timeout.ToString() + "ms for request with SessionId: " + sessionId);
            }
            catch (Exception)
            {
                if (null != message) message.Abandon();
                throw;
            }
            finally
            {
                if (null != message)
                {
                    message.Dispose();
                }
                session.Close();
            }
        }
    }
}
