﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Azure
{
    public class Configuration : Avalier.Bus.Base.Configuration
    {
        public Configuration(IBus bus)
            : base(bus)
        {
        }

        protected override IRequestorConfiguration CreateRequestor(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            return new Requestor(configuration, requestQueueName, responseQueueName);
        }

        protected override IResponderConfiguration CreateResponder(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            return new Responder(configuration, requestQueueName, responseQueueName);
        }

        protected override IPublicationConfiguration CreatePublication(IConfiguration configuration, string topicName)
        {
            return new Publication(configuration, topicName);
        }

        protected override ISubscriptionConfiguration CreateSubscription(IConfiguration configuration, string topicName, string subscriptionName)
        {
            return new Subscription(configuration, topicName, subscriptionName);
        }
    }
}
