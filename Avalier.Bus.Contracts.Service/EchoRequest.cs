﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Contracts.Service
{
    public class EchoRequest
    {
        public string Message { get; set; }
    }
}
