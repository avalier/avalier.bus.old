﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Client.Handlers.Service
{
    public class EchoHandler : IHandler<Contracts.Service.EchoRequest, Contracts.Service.EchoResponse>
    {
        public Contracts.Service.EchoResponse Execute(Contracts.Service.EchoRequest request)
        {
            return new Contracts.Service.EchoResponse()
            {
                Message = request.Message
            };
        }
    }
}
