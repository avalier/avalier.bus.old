﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Client.Handlers.Service
{
    public class GetUtcTimeHandler : IHandler<Contracts.Service.GetUtcTimeRequest, Contracts.Service.GetUtcTimeResponse>
    {
        public Contracts.Service.GetUtcTimeResponse Execute(Contracts.Service.GetUtcTimeRequest request)
        {
            return new Contracts.Service.GetUtcTimeResponse()
            {
                At = DateTime.UtcNow
            };
        }
    }
}
