﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Client.Handlers.Sales
{
    public class OrderCreatedHandler : IHandler<Contracts.Sales.OrderCreatedNotification>
    {
        public void Execute(Contracts.Sales.OrderCreatedNotification notification)
        {
            Console.WriteLine(string.Format("Order {0} for customer {1} was created", notification.OrderGuid, notification.Customer));
        }
    }
}
