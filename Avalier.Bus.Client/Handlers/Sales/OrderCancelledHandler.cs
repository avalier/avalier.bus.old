﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Client.Handlers.Sales
{
    public class OrderCancelledHandler : IHandler<Contracts.Sales.OrderCancelledNotification>
    {
        public void Execute(Contracts.Sales.OrderCancelledNotification notification)
        {
            Console.WriteLine(string.Format("Order {0} for customer {0} was created", notification.OrderGuid));
        }
    }
}
