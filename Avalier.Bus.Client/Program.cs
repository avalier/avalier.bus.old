﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = InitialiseTibcoBus();
            Execute(bus);

            Console.ReadKey();
        }

        public static void Execute(IBus bus)
        {
            using (var session = bus.OpenSession())
            {
                var orderCreated = new Contracts.Sales.OrderCreatedNotification()
                {
                    OrderGuid = Guid.NewGuid(),
                    CreatedAt = DateTime.UtcNow,
                    Customer = "Joe Bloggs"
                };
                bus.Publish(orderCreated);

                /*/
                System.Threading.Thread.Sleep(1000);
                var response = await bus.RequestAsync<Contracts.Service.GetUtcTimeRequest, Contracts.Service.GetUtcTimeResponse>(new Contracts.Service.GetUtcTimeRequest());
                Console.WriteLine("Get UTC Time: " + response.At.ToString());
                //*/
            }
        }

        public static IBus InitialiseTibcoBus()
        {
            // DI //
            ObjectFactory.Configure(x =>
            {
                x.For<Bus.IBus>().Singleton().Use<Tibco.Bus>();
                x.For<Bus.ISessionContext>().HybridHttpOrThreadLocalScoped().Use<Base.SessionContext>();
            });

            // Bus //
            var bus = ObjectFactory.GetInstance<IBus>();
            bus.Configure(o =>
            {
                o.SetConnectionString("ServerUrl=localhost;Username=Admin");
                //o.CreateRequester("ServiceRequest", "ServiceResponse").Register("Avalier.Bus.Contracts.Service");
                //o.CreateResponder("ServiceRequest", "ServiceResponse").Register("Avalier.Bus.Contracts.Service").ScanForHandlers("Avalier.Bus.Client");
                o.CreatePublication("Sales").Register("Avalier.Bus.Contracts.Sales");
                o.CreateSubscription("Sales", "Sales").Register("Avalier.Bus.Contracts.Sales").ScanForHandlers("Avalier.Bus.Client");
            })
            .Initialise();

            return bus;
        }

        public static IBus InitialiseAzureBus() {
            // DI //
            ObjectFactory.Configure(x => {
                x.For<Bus.IBus>().Singleton().Use<Azure.Bus>();
                x.For<Bus.ISessionContext>().HybridHttpOrThreadLocalScoped().Use<Base.SessionContext>();
            });

            // Bus //
            var bus = ObjectFactory.GetInstance<IBus>();
            bus.Configure(o => {
                o.SetConnectionString("Endpoint=sb://flounder/Flounder;StsEndpoint=https://flounder:9355/Flounder;RuntimePort=9354;ManagementPort=9355");
                o.CreateRequester("ServiceRequest", "ServiceResponse").Register("Avalier.Bus.Contracts.Service");
                o.CreateResponder("ServiceRequest", "ServiceResponse").Register("Avalier.Bus.Contracts.Service").ScanForHandlers("Avalier.Bus.Client");
                o.CreatePublication("Sales").Register("Avalier.Bus.Contracts.Sales");
                o.CreateSubscription("Sales", "SalesSubscription").Register("Avalier.Bus.Contracts.Sales").ScanForHandlers("Avalier.Bus.Client");
            })
            .Initialise();

            return bus;
        }
    }
}
