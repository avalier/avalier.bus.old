﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Tibco
{
    public class Bus : Avalier.Bus.Base.Bus
    {
        private Guid _guid;
        private TIBCO.EMS.ConnectionFactory _connectionFactory;
        private TIBCO.EMS.Connection _connection;

        public Bus()
            : base()
        {
            _guid = Guid.NewGuid();
        }

        public override IConfiguration CreateConfiguration(IBus bus)
        {
            return new Configuration(bus);
        }

        internal TIBCO.EMS.Connection Connection
        {
            get { return _connection; }
        }

        protected override void InitialiseBus()
        {
            var configuration = (Configuration)this.Configuration;

            // Connection Factory //
            _connectionFactory = new TIBCO.EMS.ConnectionFactory(configuration.ServerUrl);
            _connectionFactory.SetClientID(Guid.NewGuid().ToString());

            // Tibco Connection //
            _connection = _connectionFactory.CreateConnection(configuration.Username, configuration.Password);
            _connection.Start();
        }

        public override void Terminate()
        {
            if (_connection != null)
            {
                if (!_connection.IsClosed)
                {
                    _connection.Close();
                }
                _connection = null;
            }

            if (_connectionFactory != null)
            {
                _connectionFactory = null;
            }
        }
    }
}
