﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Tibco
{
    public class Requestor : Avalier.Bus.Base.Requestor
    {
        public Requestor(IConfiguration configuration, string requestQueueName, string responseQueueName)
            : base(configuration, requestQueueName, responseQueueName)
        {
        }

        public override void Initialise()
        {
        }

        public override Task<TResponse> Request<TRequest, TResponse>(TRequest request)
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;
            var connection = bus.Connection;

            TIBCO.EMS.Session session = null;
            TIBCO.EMS.MessageProducer producer = null;
            TIBCO.EMS.MessageConsumer consumer = null;

            try
            {
                session = connection.CreateSession(false, TIBCO.EMS.Session.AUTO_ACKNOWLEDGE);
                var requestQueue = session.CreateQueue(this.RequestQueueName);
                var responseQueue = session.CreateQueue(this.ResponseQueueName);
                producer = session.CreateProducer(requestQueue);
                var message = session.CreateTextMessage();
                message.MsgType = typeof(TRequest).AssemblyQualifiedName + ";" + typeof(TResponse).AssemblyQualifiedName;
                message.Text = request.ToXml();
                producer.Send(message);
                return Task.FromResult(default(TResponse));
            }
            finally
            {
                if (null != producer) producer.Close();
                if (null != consumer) consumer.Close();
                if (null != session) if (!session.IsClosed) session.Close();
            }
        }
    }
}
