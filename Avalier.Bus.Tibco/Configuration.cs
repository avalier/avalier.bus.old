﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Tibco
{
    public class Configuration : Avalier.Bus.Base.Configuration
    {
        public Configuration(IBus bus)
            : base(bus)
        {
        }

        internal string ServerUrl
        {
            get { return GetSetting("ServerUrl"); }
        }

        internal string Username
        {
            get { return GetSetting("Username"); }
        }

        internal string Password
        {
            get { return GetSetting("Password"); }
        }

        private string GetSetting(string parameterName)
        {
            var name = parameterName.ToLower() + "=";
            return this.ConnectionString.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(s => s.Trim())
                    .Where(s => s.ToLower().StartsWith(name))
                    .Select(s => s.Substring(name.Length).Trim())
                    .FirstOrDefault();
        }

        protected override IRequestorConfiguration CreateRequestor(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            return new Requestor(configuration, requestQueueName, responseQueueName);
        }

        protected override IResponderConfiguration CreateResponder(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            return new Responder(configuration, requestQueueName, responseQueueName);
        }

        protected override IPublicationConfiguration CreatePublication(IConfiguration configuration, string topicName)
        {
            return new Publication(configuration, topicName);
        }

        protected override ISubscriptionConfiguration CreateSubscription(IConfiguration configuration, string topicName, string subscriptionName)
        {
            return new Subscription(configuration, topicName, subscriptionName);
        }
    }
}
