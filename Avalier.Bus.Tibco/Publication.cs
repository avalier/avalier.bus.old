﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Tibco
{
    public class Publication : Avalier.Bus.Base.Publication
    {
        public Publication(IConfiguration configuration, string topicName)
            : base(configuration, topicName)
        {
        }

        public override void Initialise()
        {
        }

        public override void Publish<TNotification>(TNotification notification)
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;
            var connection = bus.Connection;

            TIBCO.EMS.Session session = null;
            TIBCO.EMS.MessageProducer producer = null;

            try
            {
                session = connection.CreateSession(false, TIBCO.EMS.Session.AUTO_ACKNOWLEDGE);
                var queue = session.CreateQueue(this.TopicName);
                producer = session.CreateProducer(queue);
                var message = session.CreateTextMessage();
                message.MsgType = typeof(TNotification).AssemblyQualifiedName;
                message.Text = notification.ToXml();
                producer.Send(message);
            }
            finally
            {
                if (null != producer) producer.Close();
                if (null != session) if (!session.IsClosed) session.Close();
            }
        }

    }
}
