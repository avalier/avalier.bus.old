﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Tibco
{
    public class Subscription : Avalier.Bus.Base.Subscription
    {
        private TIBCO.EMS.Session _session = null;
        private TIBCO.EMS.MessageConsumer _consumer = null;

        public Subscription(IConfiguration configuration, string topicName, string subscriptionName)
            : base(configuration, topicName, subscriptionName)
        {
        }

        public override void Initialise()
        {
            var configuration = (Configuration)this.Configuration;
            var bus = (Bus)configuration.Bus;
            var connection = bus.Connection;

            _session = connection.CreateSession(false, TIBCO.EMS.Session.AUTO_ACKNOWLEDGE);
            var queue = _session.CreateQueue(this.SubscriptionName);
            _consumer = _session.CreateConsumer(queue);
            _consumer.MessageHandler += consumer_MessageHandler;
        }

        void consumer_MessageHandler(object sender, TIBCO.EMS.EMSMessageEventArgs args)
        {
            TIBCO.EMS.TextMessage message = (TIBCO.EMS.TextMessage)args.Message;
            var notificationType = Type.GetType(message.MsgType);

            Console.WriteLine(string.Format("[ Bus ] {3}({2}) Subscription({0}): Notification Arrived",
                this.TopicName,
                this.SubscriptionName,
                notificationType.Name,
                message.MessageID
            ));

            var request = message.Text.FromXml(notificationType);
            this.Execute(notificationType, message.MessageID, request);

            Console.WriteLine(string.Format("[ Bus ] {3}({2}) Subscription({0}): Notification Completed",
                this.TopicName,
                this.SubscriptionName,
                notificationType.Name,
                message.MessageID
            ));
        }
    }
}
