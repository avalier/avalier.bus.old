﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Avalier.Bus
{
    public static class Extensions
    {

        public static string ToXml<T>(this T value)
        {
            return value.ToXml(typeof(T));
        }

        public static string ToXml(this object value, Type type)
        {
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            new XmlSerializer(type).Serialize(sw, value);
            return sb.ToString();
        }

        public static T FromXml<T>(this string value)
        {
            return (T)value.FromXml(typeof(T));
        }

        public static object FromXml(this string value, Type type)
        {
            var sr = new StringReader(value);
            return new XmlSerializer(type).Deserialize(sr);
        }
    }
}
