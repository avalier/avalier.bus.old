﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IRequestorConfiguration
    {
        IRequestorConfiguration Register<TRequest, TResponse>();

        IRequestorConfiguration Register(string assemblyName);

        IRequestorConfiguration Register(Assembly assembly);
    }
}
