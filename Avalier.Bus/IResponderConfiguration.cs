﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IResponderConfiguration
    {
        IResponderConfiguration Register<TRequest, TResponse>(Func<TRequest, TResponse> handler = null);

        IResponderConfiguration Register(string assemblyName);

        IResponderConfiguration Register(Assembly assembly);

        IResponderConfiguration ScanForHandlers(string assemblyName);

        IResponderConfiguration ScanForHandlers(Assembly assembly);
    }
}
