﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IBus : IDisposable
    {
        ISession OpenSession();

        Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request);

        void Publish<TNotification>(TNotification notification);

        IBus Configure(Action<IConfiguration> @delegate);

        IBus Initialise();

        void Terminate();
    }
}
