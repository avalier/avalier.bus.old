﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Requestor : IRequestorConfiguration
    {
        public int Timeout { get; set; }

        private IConfiguration _configuration;
        private string _requestQueueName;
        private string _responseQueueName;
        private IList<KeyValuePair<Type, Type>> _registeredRequests;

        public Requestor(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            this.Timeout = 10000;
            _configuration = configuration;
            _requestQueueName = requestQueueName;
            _responseQueueName = responseQueueName;
            _registeredRequests = new List<KeyValuePair<Type, Type>>();
        }

        public IConfiguration Configuration { get { return _configuration; } }

        public string RequestQueueName { get { return _requestQueueName; } }

        public string ResponseQueueName { get { return _responseQueueName; } }

        public IRequestorConfiguration Register<TRequest, TResponse>()
        {
            return this.Register(typeof(TRequest), typeof(TResponse));
        }

        public IRequestorConfiguration Register(Type requestType, Type responseType)
        {
            var requests = _registeredRequests
                .Where(kvp => kvp.Key == requestType)
                .Where(kvp => kvp.Value == responseType)
                .ToList();

            if (!requests.Any())
            {
                _registeredRequests.Add(new KeyValuePair<Type, Type>(requestType, responseType));
            }

            return this;
        }
        
        public IRequestorConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public IRequestorConfiguration Register(Assembly assembly)
        {
            var requestTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Request"))
                .ToList();

            var responseTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Response"))
                .ToList();

            foreach (var requestType in requestTypes)
            {
                var responseType = responseTypes
                    .Where(t => t.Name.Substring(0, t.Name.Length - 7) == requestType.Name.Substring(0, t.Name.Length - 7))
                    .First();

                if (responseType != null)
                {
                    this.Register(requestType, responseType);
                }
            }

            Console.WriteLine(string.Format("[ Bus ] Requestor({0}, {1}): Registered notifications in assembly {2}", this.RequestQueueName, this.ResponseQueueName, assembly.GetName().Name));
            return this;
        }

        internal bool Supports<TRequest, TResponse>()
        {
            return _registeredRequests
                .Where(kvp => kvp.Key == typeof(TRequest))
                .Where(kvp => kvp.Value == typeof(TResponse))
                .Any();
        }

        public abstract void Initialise();

        public abstract Task<TResponse> Request<TRequest, TResponse>(TRequest request);

    }
}
