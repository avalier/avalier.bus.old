﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Configuration : IConfiguration
    {
        private readonly IBus _bus;
        private string _connectionString = "";
        private IList<IRequestorConfiguration> _requestors;
        private IList<IResponderConfiguration> _responders;
        private IList<IPublicationConfiguration> _publications;
        private IList<ISubscriptionConfiguration> _subscriptions;

        public Configuration(IBus bus)
        {
            _bus = bus;
            _requestors = new List<IRequestorConfiguration>();
            _responders = new List<IResponderConfiguration>();
            _publications = new List<IPublicationConfiguration>();
            _subscriptions = new List<ISubscriptionConfiguration>();
        }

        public string ConnectionString  
        {
            get { return _connectionString; }
        }

        public void SetConnectionString(string connectionString)
        {
            _connectionString = connectionString;
            Console.WriteLine(string.Format("[ Bus ] {0}", this.ConnectionString));
        }

        public IRequestorConfiguration CreateRequester(string requestQueueName, string responseQueueName, Action<IRequestorConfiguration> @delegate = null)
        {
            var requestorConfiguration = this.CreateRequestor((IConfiguration)this, requestQueueName, responseQueueName);
            if (null != @delegate) @delegate(requestorConfiguration);
            _requestors.Add(requestorConfiguration);
            return requestorConfiguration;
        }

        public IResponderConfiguration CreateResponder(string requestQueueName, string responseQueueName, Action<IResponderConfiguration> @delegate = null)
        {
            var responderConfiguration = this.CreateResponder(this, requestQueueName, responseQueueName);
            if (null != @delegate) @delegate(responderConfiguration);
            _responders.Add(responderConfiguration);
            return responderConfiguration;
        }

        public IPublicationConfiguration CreatePublication(string topicName, Action<IPublicationConfiguration> @delegate = null)
        {
            var publicationConfiguration = this.CreatePublication(this, topicName);
            if (null != @delegate) @delegate(publicationConfiguration);
            _publications.Add(publicationConfiguration);
            return publicationConfiguration;
        }

        public ISubscriptionConfiguration CreateSubscription(string topicName, string subscriptionName, Action<ISubscriptionConfiguration> @delegate = null)
        {
            var subscriptionConfiguration = this.CreateSubscription(this, topicName, subscriptionName);
            if (null != @delegate) @delegate(subscriptionConfiguration);
            _subscriptions.Add(subscriptionConfiguration);
            return subscriptionConfiguration;
        }

        public IList<IPublicationConfiguration> Publications
        {
            get { return _publications; }
        }

        public IList<ISubscriptionConfiguration> Subscriptions
        {
            get { return _subscriptions; }
        }

        public IList<IRequestorConfiguration> Requestors
        {
            get { return _requestors; }
        }

        public IList<IResponderConfiguration> Responders
        {
            get { return _responders; }
        }

        public IBus Bus
        {
            get { return _bus; }
        }

        protected abstract IRequestorConfiguration CreateRequestor(IConfiguration configuration, string requestQueueName, string responseQueueName);

        protected abstract IResponderConfiguration CreateResponder(IConfiguration configuration, string requestQueueName, string responseQueueName);

        protected abstract IPublicationConfiguration CreatePublication(IConfiguration configuration, string topicName);

        protected abstract ISubscriptionConfiguration CreateSubscription(IConfiguration configuration, string topicName, string subscriptionName);

    }
}
