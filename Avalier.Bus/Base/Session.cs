﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public class Session : ISession
    {
        private ISessionContext _sessionContext;

        public Session(ISessionContext sessionContext)
        {
            _sessionContext = sessionContext;
        }

        public void Dispose()
        {
            _sessionContext.Session = null;
        }
    }
}
