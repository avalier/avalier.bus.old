﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Bus : IBus
    {
        private bool _isConfigured = false;
        private bool _isInitialised = false;
        private IConfiguration _configuration;

        public Bus()
        {
            Console.WriteLine("[ Bus ] " + this.GetType().ToString());
        }

        protected IConfiguration Configuration
        {
            get { return _configuration; }
        }

        public virtual ISessionContext SessionContext
        {
            get { return ObjectFactory.GetInstance<ISessionContext>(); }
        }

        public virtual ISession OpenSession()
        {
            AssertInitialisation();
            var sessionContext = this.SessionContext;
            if (null != sessionContext.Session)
            {
                throw new Exception("There is already an active session");
            }
            sessionContext.Session = CreateSession(sessionContext);
            return sessionContext.Session;
        }

        public virtual ISession CreateSession(ISessionContext sessionContext)
        {
            return new Session(sessionContext);
        }

        public virtual void Publish<TNotification>(TNotification notification)
        {
            AssertInitialisation();
            AssertSessionCreated();
            foreach (var publication in ((Configuration)_configuration).Publications.Cast<Base.Publication>().Where(p => p.Supports<TNotification>()))
            {
                publication.Publish(notification);
            }
        }

        public async Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request)
        {
            AssertInitialisation();
            AssertSessionCreated();
            foreach (var requestor in ((Configuration)_configuration).Requestors.Cast<Base.Requestor>().Where(r => r.Supports<TRequest, TResponse>()))
            {
                return await requestor.Request<TRequest, TResponse>(request);
            }

            throw new Exception("Bus could not find a handler for the specified request");
        }

        public IBus Configure(Action<IConfiguration> @delegate)
        {
            if (_isConfigured)
            {
                throw new Exception("Bus has already been configured");
            }
            _configuration = CreateConfiguration(this);
            @delegate(_configuration);
            _isConfigured = true;
            return this;
        }

        public abstract IConfiguration CreateConfiguration(IBus bus);

        public virtual IBus Initialise()
        {
            // Validate //
            if (!_isConfigured)
            {
                throw new Exception("Bus has not yet been configured");
            }
            if (_isInitialised)
            {
                throw new Exception("Bus has already been initialised");
            }

            // Initialise //
            InitialiseBus();
            InitialiseResponders();
            InitialiseRequestors();
            InitialisePublications();
            InitialiseSubscriptions();

            _isInitialised = true;
            Console.WriteLine("[ Bus ] Initialised");

            return this;
        }

        public virtual void Terminate()
        {
        }

        protected virtual void InitialiseBus()
        {
        }

        protected virtual void InitialiseResponders()
        {
            foreach (var responder in ((Configuration)_configuration).Responders.Cast<Base.Responder>()) responder.Initialise();
        }

        protected virtual void InitialiseRequestors()
        {
            foreach (var requestor in ((Configuration)_configuration).Requestors.Cast<Base.Requestor>()) requestor.Initialise();
        }

        protected virtual void InitialisePublications()
        {
            foreach (var publication in ((Configuration)_configuration).Publications.Cast<Base.Publication>()) publication.Initialise();
        }

        protected virtual void InitialiseSubscriptions()
        {
            foreach (var subscription in ((Configuration)_configuration).Subscriptions.Cast<Base.Subscription>()) subscription.Initialise();
        }

        protected void AssertInitialisation()
        {
            if (!_isInitialised)
            {
                throw new Exception("Bus has not been initialised");
            }
        }

        protected void AssertSessionCreated()
        {
            if (null == this.SessionContext.Session)
            {
                throw new Exception("Bus session has not been created. Please call OpenSession first.");
            }
        }

        public void Dispose()
        {
            this.Terminate();
        }
    }
}
