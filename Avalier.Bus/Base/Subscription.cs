﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Subscription : ISubscriptionConfiguration
    {
        private IConfiguration _configuration;
        private string _topicName;
        private string _subscriptionName;
        private IDictionary<Type, IList<object>> _handlers;

        public Subscription(IConfiguration configuration, string topicName, string subscriptionName)
        {
            _configuration = configuration;
            _topicName = topicName;
            _subscriptionName = subscriptionName;
            _handlers = new Dictionary<Type, IList<object>>();
        }

        public IConfiguration Configuration { get { return _configuration; } }

        public string TopicName { get { return _topicName; } }

        public string SubscriptionName { get { return _subscriptionName; } }

        public ISubscriptionConfiguration Register<TNotification>(Action<TNotification> handler = null)
        {
            return this.Register(typeof(TNotification), handler);
        }

        private ISubscriptionConfiguration Register(Type notificationType, object handler = null)
        {
            if (!_handlers.ContainsKey(notificationType))
            {
                _handlers[notificationType] = new List<object>();
            }
            if (null != handler)
            {
                _handlers[notificationType].Add(handler);
            }
            return this;
        }

        public ISubscriptionConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public ISubscriptionConfiguration Register(Assembly assembly)
        {
            var notificationTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Notification"))
                .ToList();

            foreach (var notificationType in notificationTypes)
            {
                this.Register(notificationType);
            }

            Console.WriteLine(string.Format("[ Bus ] Subscription({0}): Registered notifications in assembly {1}", this.TopicName, assembly.GetName().Name));
            return this;
        }

        public ISubscriptionConfiguration ScanForHandlers(string assemblyName)
        {
            return this.ScanForHandlers(Assembly.Load(assemblyName));
        }

        public ISubscriptionConfiguration ScanForHandlers(Assembly assembly)
        {
            var handlerDefinitions = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Select(t => {
                    foreach(var i in t.GetInterfaces()) {
                        if (i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IHandler<>)) {
                            return new {
                                HandlerType = t,
                                HandlerInterface = i,
                                NotificationType = i.GetGenericArguments()[0]
                            };
                        }
                    }
                    return null;
                })
                .Where(o => o != null)
                .ToList();

            foreach (var handlerDefinition in handlerDefinitions) {
                this.Register(handlerDefinition.NotificationType, handlerDefinition.HandlerType);
            }

            Console.WriteLine(string.Format("[ Bus ] Subscription({0}): Registered handlers in assembly {1}", this.TopicName, assembly.GetName().Name));
            return this;
        }

        protected void Execute<TNotification>(string messageId, TNotification notification)
        {
            this.Execute(typeof(TNotification), messageId, notification);
        }

        protected void Execute(Type notificationType, string messageId, object notification)
        {
            if (_handlers.ContainsKey(notificationType))
            {
                var handlers = _handlers[notificationType];
                foreach (var o in handlers)
                {
                    if (o.GetType().IsGenericType && o.GetType().GetGenericTypeDefinition() == typeof(Action<>))
                    {
                        //var handler = (Action<TNotification>)o;
                        //handler(notification);
                    }
                    else
                    {
                        //var handler = (IHandler<TNotification>)(ObjectFactory.GetInstance((Type)o));
                        //handler.Execute(notification);

                        Console.WriteLine(string.Format("[ Bus ] {3}({2}) Subscription({0}): Dispatching to " + ((Type)o).Name,
                            this.TopicName,
                            this.SubscriptionName,
                            notificationType.Name,
                            messageId
                        ));

                        var handler = ObjectFactory.GetInstance((Type)o);
                        ((Type)o).InvokeMember("Execute", BindingFlags.Default | BindingFlags.InvokeMethod, null, handler, new object[] { notification });
                    }

                }
            }
        }

        public abstract void Initialise();

    }
}
