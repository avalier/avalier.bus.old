﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Responder : IResponderConfiguration
    {
        private IConfiguration _configuration;
        private string _requestQueueName;
        private string _responseQueueName;
        private IDictionary<Type, IDictionary<Type, IList<object>>> _handlers;

        public Responder(IConfiguration configuration, string requestQueueName, string responseQueueName)
        {
            _configuration = configuration;
            _requestQueueName = requestQueueName;
            _responseQueueName = responseQueueName;
            _handlers = new Dictionary<Type, IDictionary<Type, IList<object>>>();
        }
        
        public IConfiguration Configuration { get { return _configuration; } }

        public string RequestQueueName { get { return _requestQueueName; } }

        public string ResponseQueueName { get { return _responseQueueName; } }

        public IResponderConfiguration Register<TRequest, TResponse>(Func<TRequest, TResponse> handler = null)
        {
            return this.Register(typeof(TRequest), typeof(TResponse), handler);
        }

        private IResponderConfiguration Register(Type requestType, Type responseType, object handler = null)
        {
            if (!_handlers.ContainsKey(requestType))
            {
                _handlers[requestType] = new Dictionary<Type, IList<object>>();
            }
            var subHandlers = _handlers[requestType];
            if (!subHandlers.ContainsKey(responseType))
            {
                subHandlers[responseType] = new List<object>();
            }
            if (null != handler)
            {
                _handlers[requestType][responseType].Add(handler);
            }
            return this;
        }

        public IResponderConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public IResponderConfiguration Register(Assembly assembly)
        {
            var requestTypes = assembly.GetTypes()
                 .Where(t => t.IsClass)
                 .Where(t => t.Name.EndsWith("Request"))
                 .ToList();

            var responseTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Response"))
                .ToList();

            foreach (var requestType in requestTypes)
            {
                var responseType = responseTypes
                    .Where(t => t.Name.Substring(0, t.Name.Length - 7) == requestType.Name.Substring(0, t.Name.Length - 7))
                    .First();

                if (responseType != null)
                {
                    this.Register(requestType, responseType);
                }
            }

            Console.WriteLine(string.Format("[ Bus ] Responder({0}, {1}): Registered notifications in assembly {2}", this.RequestQueueName, this.ResponseQueueName, assembly.GetName().Name));
            return this;
        }

        public IResponderConfiguration ScanForHandlers(string assemblyName)
        {
            return this.ScanForHandlers(Assembly.Load(assemblyName));
        }

        public IResponderConfiguration ScanForHandlers(Assembly assembly)
        {
            var handlerDefinitions = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Select(t => {
                    foreach(var i in t.GetInterfaces()) {
                        if (i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IHandler<,>)) {
                            return new {
                                HandlerType = t,
                                HandlerInterface = i,
                                RequestType = i.GetGenericArguments()[0],
                                ResponseType = i.GetGenericArguments()[1]
                            };
                        }
                    }
                    return null;
                })
                .Where(o => o != null)
                .ToList();

            foreach (var handlerDefinition in handlerDefinitions)
            {
                this.Register(handlerDefinition.RequestType, handlerDefinition.ResponseType, handlerDefinition.HandlerType);
            }

            Console.WriteLine(string.Format("[ Bus ] Responder({0}, {1}): Registered handlers in assembly {2}", this.RequestQueueName, this.ResponseQueueName, assembly.GetName().Name));
            return this;
        }

        public TResponse Execute<TRequest, TResponse>(string messageId, TRequest request)
        {
            return (TResponse)this.Execute(typeof(TRequest), typeof(TResponse), messageId, request);
        }

        public object Execute(Type requestType, Type responseType, string messageId, object request)
        {
            if (_handlers.ContainsKey(requestType))
            {
                var subHandlers = _handlers[requestType];
                if (subHandlers.ContainsKey(responseType))
                {
                    var handlers = subHandlers[responseType];
                    foreach (var o in handlers)
                    {
                        if (o.GetType().IsGenericType && o.GetType().GetGenericTypeDefinition() == typeof(Func<,>))
                        {
                            //var handler = (Func<TRequest, TResponse>)o;
                            //return handler(request);
                        }
                        else
                        {
                            //var handler = (IHandler<TRequest, TResponse>)(ObjectFactory.GetInstance((Type)o));
                            //return handler.Execute(request);

                            Console.WriteLine(string.Format("[ Bus ] {4}({2}:{3}) Responder({0}, {1}): Dispatching to " + ((Type)o).Name,
                                this.RequestQueueName,
                                this.ResponseQueueName,
                                requestType.Name,
                                responseType.Name,
                                messageId
                            ));

                            var handler = ObjectFactory.GetInstance((Type)o);
                            var response = ((Type)o).InvokeMember("Execute", BindingFlags.Default | BindingFlags.InvokeMethod, null, handler, new object[] { request });
                            return response;
                        }
                    }
                }
            }
            throw new Exception("Could not find a hander for Request: " + requestType.Name + ", Response: " + responseType.Name);
        }

        public abstract void Initialise();

    }
}
