﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Base
{
    public abstract class Publication : IPublicationConfiguration
    {
        private IConfiguration _configuration;
        private string _topicName;
        private IList<Type> _registeredNotifications;

        public Publication(IConfiguration configuration, string topicName)
        {
            _configuration = configuration;
            _topicName = topicName;
            _registeredNotifications = new List<Type>();
        }

        public IConfiguration Configuration { get { return _configuration; } }

        public string TopicName { get { return _topicName; } }

        public IPublicationConfiguration Register<TNotification>()
        {
            return this.Register(typeof(TNotification));
        }

        private IPublicationConfiguration Register(Type notificationType)
        {
            if (!_registeredNotifications.Where(t => t == notificationType).Any())
            {
                _registeredNotifications.Add(notificationType);
            }
            return this;
        }

        public IPublicationConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public IPublicationConfiguration Register(Assembly assembly)
        {
            var notificationTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Notification"))
                .ToList();

            foreach (var notificationType in notificationTypes)
            {
                this.Register(notificationType);
            }

            Console.WriteLine(string.Format("[ Bus ] Publication({0}): Registered notifications in assembly {1}", this.TopicName, assembly.GetName().Name));
            return this;
        }

        public bool Supports<TNotification>()
        {
            return _registeredNotifications.Contains(typeof(TNotification));
        }

        public abstract void Publish<TNotification>(TNotification notification);

        public abstract void Initialise();
    }
}
