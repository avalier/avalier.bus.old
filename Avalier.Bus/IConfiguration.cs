﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IConfiguration
    {
        string ConnectionString { get; }

        void SetConnectionString(string connectionString);

        IPublicationConfiguration CreatePublication(string topicName, Action<IPublicationConfiguration> @delegate = null);

        ISubscriptionConfiguration CreateSubscription(string topicName, string subscriptionName, Action<ISubscriptionConfiguration> @delegate = null);

        IRequestorConfiguration CreateRequester(string requestQueueName, string responseQueueName, Action<IRequestorConfiguration> @delegate = null);

        IResponderConfiguration CreateResponder(string requestQueueName, string responseQueueName, Action<IResponderConfiguration> @delegate = null);

        /*/
         * 
        IList<IPublicationConfiguration> Publications { get; }

        IList<ISubscriptionConfiguration> Subscriptions { get; }

        IList<IRequestorConfiguration> Requestors { get; }

        IList<IResponderConfiguration> Responders { get; }

        //*/
    }
}
