﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface ISessionContext
    {
        ISession Session { get; set; }
    }
}
