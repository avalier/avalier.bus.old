﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IHandler<TNotification>
    {
        void Execute(TNotification notification);
    }

    public interface IHandler<TRequest, TResponse>
    {
        TResponse Execute(TRequest request);
    }
}
