﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface IPublicationConfiguration
    {
        IPublicationConfiguration Register<TNotification>();

        IPublicationConfiguration Register(string assemblyName);

        IPublicationConfiguration Register(Assembly assembly);
    }
}
