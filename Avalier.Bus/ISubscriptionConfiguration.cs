﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus
{
    public interface ISubscriptionConfiguration
    {
        ISubscriptionConfiguration Register<TNotification>(Action<TNotification> handler = null);

        ISubscriptionConfiguration Register(string assemblyName);

        ISubscriptionConfiguration Register(Assembly assembly);

        ISubscriptionConfiguration ScanForHandlers(string assemblyName);

        ISubscriptionConfiguration ScanForHandlers(Assembly assembly);
    }
}
