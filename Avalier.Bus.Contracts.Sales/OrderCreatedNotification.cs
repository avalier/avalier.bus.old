﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Contracts.Sales
{
    public class OrderCreatedNotification
    {
        public OrderCreatedNotification()
        {
            this.Lines = new List<string>();
        }

        public Guid OrderGuid { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Customer { get; set; }

        public List<string> Lines { get; set; }
    }
}
