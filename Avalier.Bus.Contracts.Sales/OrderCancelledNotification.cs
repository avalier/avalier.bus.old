﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Contracts.Sales
{
    public class OrderCancelledNotification
    {
        public Guid OrderGuid { get; set; }
    }
}
