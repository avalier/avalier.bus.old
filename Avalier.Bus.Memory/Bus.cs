﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Memory
{
    public class Bus : IBus
    {
        private bool _isConfigured = false;
        private bool _isInitialised = false;
        private Configuration _configuration;

        public Bus()
        {
            Console.WriteLine("[ Bus ] " + this.GetType().ToString());
        }

        private SessionContext SessionContext
        {
            get { return ObjectFactory.GetInstance<ISessionContext>() as SessionContext; }
        }

        public ISession OpenSession()
        {
            AssertInitialisation();
            var sessionContext = this.SessionContext;
            if (null != sessionContext.Session)
            {
                throw new Exception("There is already an active session");
            }
            sessionContext.Session = new Session(sessionContext);
            return sessionContext.Session;
        }

        public void Publish<TNotification>(TNotification notification)
        {
            AssertInitialisation();
            AssertSessionCreated();
            foreach (var publication in _configuration.Publications.Where(p => p.Supports <TNotification>()))
            {
                foreach (var subscription in _configuration.Subscriptions.Where(s => s.QueueName == publication.QueueName))
                {
                    subscription.Publish(notification);
                }
            }
        }

        public async Task<TResponse> RequestAsync<TRequest, TResponse>(TRequest request)
        {
            AssertInitialisation();
            AssertSessionCreated();
            foreach (var requestor in _configuration.Requestors.Where(r => r.Supports<TRequest, TResponse>()))
            {
                return await requestor.RequestAsync<TRequest, TResponse>(request);
            }

            throw new Exception("Bus could not find a handler for the specified request");
        }

        public IBus Configure(Action<IConfiguration> @delegate)
        {
            if (_isConfigured)
            {
                throw new Exception("Bus has already been configured");
            }
            _configuration = new Configuration(this);
            @delegate(_configuration);
            _isConfigured = true;
            return this;
        }

        public IBus Initialise()
        {
            if (!_isConfigured)
            {
                throw new Exception("Bus has not yet been configured");
            }
            if (_isInitialised)
            {
                throw new Exception("Bus has already been initialised");
            }
            _isInitialised = true;
            return this;
        }

        protected void AssertInitialisation()
        {
            if (!_isInitialised)
            {
                throw new Exception("Bus has not been initialised");
            }
        }

        protected void AssertSessionCreated()
        {
            if (null == this.SessionContext.Session)
            {
                throw new Exception("Bus session has not been created. Please call OpenSession first.");
            }
        }

        public void Terminate() { }

        public void Dispose() { }
    }
}
