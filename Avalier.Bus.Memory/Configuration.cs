﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Memory
{
    public class Configuration : IConfiguration
    {
        private readonly Bus _bus;
        private string _connectionString;
        private IList<Publication> _publications;
        private IList<Subscription> _subscriptions;
        private IList<Requestor> _requestors;
        private IList<Responder> _responders;

        public Configuration(Bus bus)
        {
            _bus = bus;
            _publications = new List<Publication>();
            _subscriptions = new List<Subscription>();
            _requestors = new List<Requestor>();               
            _responders = new List<Responder>();
        }

        public IBus Bus
        {
            get { return _bus; }
        }

        public string ConnectionString  
        {
            get { return _connectionString; }
        }

        public void SetConnectionString(string connectionString)
        {
            _connectionString = connectionString;
            Console.WriteLine(string.Format("[ Bus ] {0}", this.ConnectionString));
        }

        public IPublicationConfiguration CreatePublication(string topicName, Action<IPublicationConfiguration> @delegate = null)
        {
            var publicationConfiguration = new Publication(this, topicName);
            if (null != @delegate) @delegate(publicationConfiguration);
            _publications.Add(publicationConfiguration);
            return publicationConfiguration;
        }

        public ISubscriptionConfiguration CreateSubscription(string topicName, string subscriptionName, Action<ISubscriptionConfiguration> @delegate = null)
        {
            var subscriptionConfiguration = new Subscription(this, topicName, subscriptionName);
            if (null != @delegate) @delegate(subscriptionConfiguration);
            _subscriptions.Add(subscriptionConfiguration);
            return subscriptionConfiguration;
        }

        public IRequestorConfiguration CreateRequester(string requestQueueName, string responseQueueName, Action<IRequestorConfiguration> @delegate = null)
        {
            var requestorConfiguration = new Requestor(this, requestQueueName, responseQueueName);
            if (null != @delegate) @delegate(requestorConfiguration);
            _requestors.Add(requestorConfiguration);
            return requestorConfiguration;
        }

        public IResponderConfiguration CreateResponder(string requestQueueName, string responseQueueName, Action<IResponderConfiguration> @delegate = null)
        {
            var responderConfiguration = new Responder(this, requestQueueName, responseQueueName);
            if (null != @delegate) @delegate(responderConfiguration);
            _responders.Add(responderConfiguration);
            return responderConfiguration;
        }

        internal IList<Publication> Publications
        {
            get { return _publications; }
        }

        internal IList<Subscription> Subscriptions
        {
            get { return _subscriptions; }
        }

        internal IList<Requestor> Requestors
        {
            get { return _requestors; }
        }

        internal IList<Responder> Responders
        {
            get { return _responders; }
        }
    }
}
