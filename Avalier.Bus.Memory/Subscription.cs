﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Memory
{
    public class Subscription : ISubscriptionConfiguration
    {
        private Configuration _configuration;
        private string _queueName;
        private string _subscriptionName;
        private IDictionary<Type, IList<object>> _handlers;

        public Subscription(Configuration configuration, string queueName, string subscriptionName)
        {
            _configuration = configuration;
            _queueName = queueName;
            _subscriptionName = subscriptionName;
            _handlers = new Dictionary<Type, IList<object>>();
            //Console.WriteLine(string.Format("[ Bus ] Consumer({0}): Created", this.QueueName));
        }

        public string QueueName { get { return _queueName; } }

        public ISubscriptionConfiguration Register<TNotification>(Action<TNotification> handler = null)
        {
            return this.Register(typeof(TNotification), handler);
        }

        private ISubscriptionConfiguration Register(Type notificationType, object handler = null)
        {
            if (!_handlers.ContainsKey(notificationType))
            {
                _handlers[notificationType] = new List<object>();
            }
            if (null != handler)
            {
                _handlers[notificationType].Add(handler);
            }
            return this;
        }

        public ISubscriptionConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public ISubscriptionConfiguration Register(Assembly assembly)
        {
            var notificationTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Notification"))
                .ToList();

            foreach (var notificationType in notificationTypes)
            {
                this.Register(notificationType);
            }

            Console.WriteLine(string.Format("[ Bus ] Consumer({0}): Registered notifications in assembly {1}", this.QueueName, assembly.GetName().Name));
            return this;
        }

        public ISubscriptionConfiguration ScanForHandlers(string assemblyName)
        {
            return this.ScanForHandlers(Assembly.Load(assemblyName));
        }

        public ISubscriptionConfiguration ScanForHandlers(Assembly assembly)
        {
            var handlerDefinitions = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Select(t => {
                    foreach(var i in t.GetInterfaces()) {
                        if (i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IHandler<>)) {
                            return new {
                                HandlerType = t,
                                HandlerInterface = i,
                                NotificationType = i.GetGenericArguments()[0]
                            };
                        }
                    }
                    return null;
                })
                .Where(o => o != null)
                .ToList();

            foreach (var handlerDefinition in handlerDefinitions) {
                this.Register(handlerDefinition.NotificationType, handlerDefinition.HandlerType);
            }

            Console.WriteLine(string.Format("[ Bus ] Consumer({0}): Registered handlers in assembly {1}", this.QueueName, assembly.GetName().Name));
            return this;
        }

        internal void Publish<TNotification>(TNotification notification)
        {
            if (_handlers.ContainsKey(typeof(TNotification)))
            {
                var handlers = _handlers[typeof(TNotification)];
                foreach (var o in handlers)
                {
                    if (o.GetType().IsGenericType && o.GetType().GetGenericTypeDefinition() == typeof(Action<>))
                    {
                        var handler = (Action<TNotification>)o;
                        handler(notification);
                    }
                    else
                    {
                        var handler = (IHandler<TNotification>)(ObjectFactory.GetInstance((Type)o));
                        handler.Execute(notification);
                    }
                    
                }
            }
        }

    }
}
