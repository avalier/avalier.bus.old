﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Memory
{
    public class Publication : IPublicationConfiguration
    {
        private Configuration _configuration;
        private string _queueName;
        private IList<Type> _registeredNotifications;

        public Publication(Configuration configuration, string queueName)
        {
            _configuration = configuration;
            _queueName = queueName;
            _registeredNotifications = new List<Type>();
        }

        public string QueueName { get { return _queueName; } }

        public IPublicationConfiguration Register<TNotification>()
        {
            return this.Register(typeof(TNotification));
        }

        private IPublicationConfiguration Register(Type notificationType)
        {
            if (!_registeredNotifications.Where(t => t == notificationType).Any())
            {
                _registeredNotifications.Add(notificationType);
            }
            return this;
        }

        public IPublicationConfiguration Register(string assemblyName)
        {
            return this.Register(Assembly.Load(assemblyName));
        }

        public IPublicationConfiguration Register(Assembly assembly)
        {
            var notificationTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Notification"))
                .ToList();

            foreach (var notificationType in notificationTypes)
            {
                this.Register(notificationType);
            }

            Console.WriteLine(string.Format("[ Bus ] Producer({0}): Registered notifications in assembly {1}", this.QueueName, assembly.GetName().Name));
            return this;
        }

        internal bool Supports<TNotification>()
        {
            return _registeredNotifications.Contains(typeof(TNotification));
        }
    }
}
