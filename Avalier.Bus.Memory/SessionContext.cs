﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Bus.Memory
{
    public class SessionContext : ISessionContext
    {
        public ISession Session { get; set; }
    }
}
